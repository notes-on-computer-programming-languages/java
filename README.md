# java

Designed to have as few implementation dependencies as possible. http://oracle.com/java/

# Books
* *Functional Programming in Java, 2nd Edition*
  2023 Venkat Subramaniam
* *A Functional Approach to Java : augmenting object-oriented Java code with functional principles*
  2023 Ben Weidig
* *Java 8 in Action: Lambdas, streams, and functional-style programming*
  2019
* *Modern Java in action : lambda, streams, functional and reactive programming*
  2019
* [*Effective Java*](https://www.worldcat.org/title/effective-java/editions)
  2017-12 Joshua Bloch
  * [*Effective Java, 3rd Edition*
    ](https://www.oreilly.com/library/view/effective-java-3rd/9780134686097/)
  * [*Effective ML*](https://blog.janestreet.com/effective-ml/)
    2010-04 Yaron Minsky; inspirated by these items:
    * Minimize mutability
    * Favor composition over inheritance
    * Use function objects to represent strategies (No more on 3rd edition)

# Features
* Ahead of time compiler, see: https://gitlab.com/apt-packages-demo/default-jdk-headless

## Memory management
* [*RAII in Java*
  ](https://www.yegor256.com/2017/08/08/raii-in-java.html)
  2017-08 Yegor Bugayenko (Russian: Его́р Бугае́нко)

# Librarires
## Concurrent and parallel programming
* https://tracker.debian.org/pkg/multiverse-core (Java, Scala)

## Peer credentials unix domain socket
### junixsocket
* [*Peer Credentials*](https://kohlschutter.github.io/junixsocket/peercreds.html)
* PostgreSQL JDBC Driver, [Connecting to the Database
  ](https://jdbc.postgresql.org/documentation/head/connect.html)
* [fiken/junixsocket](https://github.com/fiken/junixsocket)
  Unix Domain Sockets in Java (AF_UNIX)
* [kohlschutter/junixsocket](https://github.com/kohlschutter/junixsocket)
  Unix Domain Sockets in Java (AF_UNIX)
  * https://tracker.debian.org/pkg/junixsocket